# Enter your code here. Read input from STDIN. Print output to STDOUT  
n = int(input().strip())
for i in range(n):
  stri = input().strip()
  arr = stri.split(' ')
  try:
    a = int(arr[0])
    b = int(arr[1])
    try:
      print(int(a/b))
    except ZeroDivisionError:
      print("Error Code: integer division or modulo by zero")
  except ValueError as e:
    print("Error Code:",e)    
